package br.com.hitss.fieldserviceapp.rest;

import android.util.Base64;
import android.util.Log;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import br.com.hitss.fieldserviceapp.model.TicketHistory;
import br.com.hitss.fieldserviceapp.model.UserFs;
import br.com.hitss.fieldserviceapp.model.UserLocationHistory;

public class UserRestClient {

    private RestTemplate restTemplate;

    public UserRestClient() {
        restTemplate = new RestTemplate();
    }

    public void postUserLocationHistory(String idUserFs, Double latitude, Double longitude) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        String plainCreds = "web.mobile:wm12345";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encode(plainCredsBytes,Base64.DEFAULT);
        String base64Creds = new String(base64CredsBytes);
        headers.add(HttpHeaders.AUTHORIZATION, "Basic " + base64Creds);

        HttpEntity<UserLocationHistory> request = new HttpEntity<UserLocationHistory>(new UserLocationHistory(latitude,longitude), headers);

        try {
            restTemplate.exchange(
                    "http://10.172.16.78:7080/fieldservice/v1/users/" + idUserFs + "/locationhistory", HttpMethod.POST, request, UserLocationHistory.class);
        } catch (Exception e) {
            Log.e("Users:", "erro ao fazer POST de UserLocationHistory.", e);
            throw e;
        }
    }

    public void postLogoff(String idUserFs) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        String plainCreds = "web.mobile:wm12345";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encode(plainCredsBytes,Base64.DEFAULT);
        String base64Creds = new String(base64CredsBytes);
        headers.add(HttpHeaders.AUTHORIZATION, "Basic " + base64Creds);

        HttpEntity request = new HttpEntity(headers);

        try {
            restTemplate.exchange(
                    "http://10.172.16.78:7080/fieldservice/v1/users/" + idUserFs + "/logout", HttpMethod.POST, request, Object.class);
        } catch (Exception e) {
            Log.e("Users:", "erro ao fazer POST de UserLocationHistory.", e);
            throw e;
        }
    }

    public UserFs login(String mEmail, String mPassword) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        String plainCreds = "web.mobile:wm12345";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encode(plainCredsBytes,Base64.DEFAULT);
        String base64Creds = new String(base64CredsBytes);
        headers.add(HttpHeaders.AUTHORIZATION, "Basic " + base64Creds);
        headers.add("login", mEmail);
        headers.add("password", mPassword);

        HttpEntity entity = new HttpEntity(headers);
        try {
            return restTemplate.exchange(
                    "http://10.172.16.78:7080/fieldservice/v1/users/login", HttpMethod.GET, entity,
                    new ParameterizedTypeReference<UserFs>() {
            }).getBody();
        } catch (Exception e) {
            Log.e("Users:", "erro ao fazer login", e);
            throw e;
        }
    }
}
